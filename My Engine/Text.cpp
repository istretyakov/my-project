#include "Text.h"

Text::Text()
{
	pVertexBuffer = nullptr;
	pIndexBuffer = nullptr;

	NumIndices = 0;
	NumSymbols = 0;
	Content = nullptr;
	pFont = nullptr;
}

void Text::SetFont(Font *font)
{
	this->pFont = font;
}

int Text::SetText(ID3D11DeviceContext *pDeviceContext, const char *text)
{
	strcpy_s(Content, strlen(text) + 1, text);

	NumIndices = strlen(text) * 6;

	pFont->BuildVertexBuffer(pDeviceContext, pVertexBuffer, text, 512, 512);

	return 0;
}

int Text::UpdateVertexBuffer(ID3D11DeviceContext *pDeviceContext)
{
	pFont->BuildVertexBuffer(pDeviceContext, pVertexBuffer, Content, 512, 512);
}

int Text::Create(ID3D11Device *pDevice, ID3D11DeviceContext *pDeviceContext, const char *text, int numSymbols)
{
	NumSymbols = numSymbols;
	NumIndices = strlen(text) * 6;
	Content = new char[numSymbols + 1];
	InitializeBuffer(pDevice);
	SetText(pDeviceContext, text);

	return 0;
}

int Text::InitializeBuffer(ID3D11Device *pDevice)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA sd;
	unsigned long *indices;

	memset(&bd, 0, sizeof(bd));
	bd.ByteWidth = sizeof(Vertex) * NumSymbols * 6;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	pDevice->CreateBuffer(&bd, NULL, &pVertexBuffer);

	bd.ByteWidth = sizeof(unsigned long) * NumSymbols * 6;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.CPUAccessFlags = 0;

	indices = new unsigned long[NumSymbols * 6];
	for (unsigned int i = 0; i < NumSymbols * 6; i++)
		indices[i] = i;

	memset(&sd, 0, sizeof(sd));
	sd.pSysMem = indices;

	pDevice->CreateBuffer(&bd, &sd, &pIndexBuffer);

	delete[] indices;

	return 0;
}

int Text::Draw(ID3D11DeviceContext *pDeviceContext, XMMATRIX *projection)
{
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	pFont->SetParameters(pDeviceContext, projection);

	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pDeviceContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);
	pDeviceContext->IASetIndexBuffer(pIndexBuffer, DXGI_FORMAT_R32_UINT, offset);

	pDeviceContext->DrawIndexed(NumIndices, 0, 0);

	return 0;
}