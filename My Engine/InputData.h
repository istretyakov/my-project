#pragma once

#include "stdafx.h"
#include "InputCodes.h"

const int MAX_KEYS = 256;
const int MAX_MOUSE_KEYS = 3;

class InputData
{
private:
	bool Keys[MAX_KEYS];
	bool MouseKeys[MAX_MOUSE_KEYS];
	POINT MousePosition;

public:
	InputData();
	
	void PutInputData(UINT msg, WPARAM wParam, LPARAM lParam);
	void SetKeyState(int key, bool state);
	void SetMouseKeyState(int key, bool state);
	void SetMousePosition(int x, int y);
	void GetMousePosition(int &x, int &y);
	bool IsKeyDown(int key);
	bool IsMouseKeyDown(int key);
};

