#include "FrameTimer.h"

bool FrameTimer::Initialize()
{
	INT64 frequency;

	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);

	if (frequency == 0)
		return true;

	TicksPerMs = (float)frequency / 1000.0f;

	QueryPerformanceCounter((LARGE_INTEGER *)&PreviousTime);

	return false;
}

void FrameTimer::Frame()
{
	INT64 currentTime;
	INT64 differentTime;

	QueryPerformanceCounter((LARGE_INTEGER *)&currentTime);

	differentTime = currentTime - PreviousTime;

	DeltaTime = differentTime / TicksPerMs;

	PreviousTime = currentTime;
}

float FrameTimer::GetDelta()
{
	return DeltaTime;
}