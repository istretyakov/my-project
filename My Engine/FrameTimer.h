#pragma once

#include "stdafx.h"

class FrameTimer
{
private:
	INT64 PreviousTime;
	float TicksPerMs;
	float DeltaTime;

public:
	bool Initialize();
	void Frame();
	float GetDelta();
};