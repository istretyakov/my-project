#include "Shader.h"

Shader::Shader()
{
	Elements = nullptr;
	NumElements = 0;
	pInputLayout = nullptr;
	pVertexShader = 0;
	pPixelShader = 0;
}

int Shader::Create(ID3D11Device *pDevice, const char *fileVSName, const char *funcVS,
	const char *filePSName, const char *funcPS)
{
	ID3DBlob *vsBlob;
	ID3DBlob *psBlob;

	D3DX11CompileFromFile(fileVSName, NULL, NULL, funcVS, "vs_4_0", 0, 0, NULL, &vsBlob, NULL, NULL);
	D3DX11CompileFromFile(filePSName, NULL, NULL, funcPS, "ps_4_0", 0, 0, NULL, &psBlob, NULL, NULL);

	pDevice->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), NULL, &pVertexShader);
	pDevice->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), NULL, &pPixelShader);

	pDevice->CreateInputLayout(Elements, NumElements, vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), &pInputLayout);

	vsBlob->Release();
	psBlob->Release();

	return 0;
}

int Shader::AddInputElement(const char *elemName, DXGI_FORMAT format)
{
	if (NumElements == 8)
		return 1;

	if (NumElements == 0)
		Elements = new D3D11_INPUT_ELEMENT_DESC[8];

	unsigned int index = NumElements;

	Elements[index].SemanticName = elemName;
	Elements[index].SemanticIndex = 0;
	Elements[index].Format = format;
	Elements[index].InputSlot = 0;
	if (!NumElements)
		Elements[index].AlignedByteOffset = 0;
	else
		Elements[index].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	Elements[index].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	Elements[index].InstanceDataStepRate = 0;

	NumElements++;

	return 0;
}

int Shader::SetParameters(ID3D11DeviceContext *pDeviceContext)
{
	pDeviceContext->IASetInputLayout(pInputLayout);
	pDeviceContext->VSSetShader(pVertexShader, NULL, 0);
	pDeviceContext->PSSetShader(pPixelShader, NULL, 0);

	return 0;
}