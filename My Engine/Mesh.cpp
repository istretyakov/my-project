#include <iostream>
#include <fstream>

#include "Mesh.h"

void Mesh::Draw(ID3D11DeviceContext *pDeviceContext)
{
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pDeviceContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);
	pDeviceContext->IASetIndexBuffer(pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	pDeviceContext->DrawIndexed(IndexCount, 0, 0);
}

void Mesh::LoadFromFile(const char *fileName)
{
	unsigned int numVertices = 0, numNormals = 0, numFaces = 0;

	ParseCount(fileName, numVertices, numNormals, numFaces);

	VertexCount = numFaces * 3;
	IndexCount = numFaces * 3;
	Vertices = new Vertex[numFaces * 3];
	Indices = new unsigned int[numFaces * 3];

	ParseData(fileName, numVertices, numNormals, numFaces);
}

void Mesh::ParseCount(const char *fileName, unsigned int &numVertices, unsigned int &numNormals, unsigned int &numFaces)
{
	std::ifstream infile;

	infile.open(fileName);

	if (infile.is_open())
	{
		char c;

		infile.get(c);

		while (!infile.eof())
		{
			if (c == 'v')
			{
				infile.get(c);
				if (c == ' ')
					numVertices++;
				else if (c == 'n')
					numNormals++;
			}
			else if (c == 'f')
				numFaces++;

			while (c != '\n')
				infile.get(c);

			infile.get(c);
		}

		infile.close();
	}
}

void Mesh::ParseData(const char *fileName, unsigned int numVertices, unsigned int numNormals, unsigned int numFaces)
{
	struct Face
	{
		unsigned int Vertices[3];
		unsigned int Normals[3];
	};

	std::ifstream infile;
	XMFLOAT3 *verticesBuffer = new XMFLOAT3[numVertices];
	XMFLOAT3 *normalsBuffer = new XMFLOAT3[numNormals];
	Face *facesBuffer = new Face[numFaces];
	unsigned int vertBuffCount = 0, normBuffCount = 0, facBuffCount = 0;
	unsigned int vertCount = 0, facCount = 0;

	infile.open(fileName);

	if (infile.is_open())
	{
		char c;

		infile.get(c);

		while (!infile.eof())
		{
			if (c == 'v')
			{
				infile.get(c);

				if (c == ' ')
				{
					float x, y, z;
					infile >> x >> y >> z;
					verticesBuffer[vertBuffCount] = XMFLOAT3(x, y, -z);
					vertBuffCount++;
				}
				else if (c == 'n')
				{
					float x, y, z;
					infile >> x >> y >> z;
					normalsBuffer[normBuffCount] = XMFLOAT3(x, y, -z);
					normBuffCount++;
				}
			}
			else if (c == 'f')
			{
				unsigned int vertIndex, normIndex;
				char s;

				for (int i = 0; i < 3; i++)
				{
					infile >> vertIndex >> c >> s >> normIndex;
					facesBuffer[facCount].Vertices[i] = vertIndex - 1;
					facesBuffer[facCount].Normals[i] = normIndex - 1;
				}

				facCount++;
			}

			while (c != '\n')
				infile.get(c);

			infile.get(c);
		}

		infile.close();

		//for (int i = 0; i < vertBuffCount; i++)
			//vertices[i].Position = verticesBuffer[i];

		for (int i = 0; i < facCount; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				Vertices[i * 3 + j].Position = verticesBuffer[facesBuffer[i].Vertices[j]];
				Vertices[i * 3 + j].Normal = normalsBuffer[facesBuffer[i].Normals[j]];
				Indices[i * 3 + j] = i * 3 + (2 - j);
			}
		}
	}

	delete[] verticesBuffer;
	delete[] normalsBuffer;
	delete[] facesBuffer;
}

void Mesh::CreateBuffers(ID3D11Device *pDevice)
{
	D3D11_BUFFER_DESC bd;
	memset(&bd, 0, sizeof(bd));
	bd.ByteWidth = sizeof(Vertex) * VertexCount;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA sd;
	memset(&sd, 0, sizeof(sd));
	sd.pSysMem = Vertices;

	pDevice->CreateBuffer(&bd, &sd, &pVertexBuffer);

	bd.ByteWidth = sizeof(unsigned int) * IndexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	sd.pSysMem = Indices;

	pDevice->CreateBuffer(&bd, &sd, &pIndexBuffer);
}