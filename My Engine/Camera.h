#pragma once

#include "stdafx.h"

class Camera
{
protected:
	XMFLOAT3 Position;
	XMFLOAT3 Rotation;

public:
	Camera();

	void SetPosition(float x, float y, float z);
	void GetPosition(float &x, float &y, float &z);
	void SetRotation(float x, float y, float z);
	void GetRotation(float &x, float &y, float &z);
	virtual XMFLOAT3 Translate(XMFLOAT3 movement) = 0;
	virtual void Rotate(XMFLOAT3 rotation) = 0;
	virtual void GetViewMatrix(XMMATRIX &view) = 0;
};