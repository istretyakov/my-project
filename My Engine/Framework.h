#pragma once

#include "Window.h"
#include "Render.h"

class Framework
{
private:
	Window *Wnd;
	Render *Rnd;

public:
	Framework();

	int Initialize(int width, int height);
	void SetRender(Render *render);
	void Run();
	void Close();
};