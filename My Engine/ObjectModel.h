#pragma once

#include "Mesh.h"
#include "ModelShader.h"

class ObjectModel
{
private:
	Mesh ModelMesh;
	ModelShader *pShader;
	XMFLOAT3 Position;
	XMFLOAT3 Rotation;

public:
	ObjectModel();

	void CreateMesh(ID3D11Device *pDevice, const char *fileName);
	void SetShader(ModelShader *pShader);
	void GetWorldMatrix(XMMATRIX &world);
	void SetPosition(float x, float y, float z);
	void GetPosition(float &x, float &y, float &z);
	void SetRotation(float x, float y, float z);
	void GetRotation(float &x, float &y, float &z);
	void Draw(ID3D11DeviceContext *pDevice, XMMATRIX *view, XMMATRIX *projection);
};