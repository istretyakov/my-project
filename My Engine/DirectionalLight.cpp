#include "DirectionalLight.h"

DirectionalLight::DirectionalLight()
{
	Diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	Ambient = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	Direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
}

void DirectionalLight::SetDirection(float x, float y, float z)
{
	Direction.x = x;
	Direction.y = y;
	Direction.z = z;
}

void DirectionalLight::GetDirection(float &x, float &y, float &z)
{
	x = Direction.x;
	y = Direction.y;
	z = Direction.z;
}

void  DirectionalLight::SetDiffuse(float r, float g, float b)
{
	Diffuse.x = r;
	Diffuse.y = g;
	Diffuse.z = b;
}

void  DirectionalLight::GetDiffuse(float &r, float &g, float &b)
{
	r = Diffuse.x;
	g = Diffuse.y;
	b = Diffuse.z;
}

void  DirectionalLight::SetAmbient(float r, float g, float b)
{
	Ambient.x = r;
	Ambient.y = g;
	Ambient.z = b;
}

void  DirectionalLight::GetAmbient(float &r, float &g, float &b)
{
	r = Ambient.x;
	g = Ambient.y;
	b = Ambient.z;
}