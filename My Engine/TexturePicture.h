#pragma once

#include "stdafx.h"
#include "TextureShader.h"

class TexturePicture
{
public:
	struct Vertex
	{
		XMFLOAT3 Position;
		XMFLOAT2 Tex;
	};

private:
	ID3D11Buffer *pVertexBuffer;
	ID3D11Buffer *pIndexBuffer;
	ID3D11ShaderResourceView *pTexture;
	
	TextureShader *pShader;
	XMFLOAT2 Position;
	unsigned long Width;
	unsigned long Height;

public:
	TexturePicture();

	void SetPosition(float x, float y);
	void SetSize(unsigned long width, unsigned long height);
	void InitializeBuffers(ID3D11Device *pDevice);
	void UpdateVertexBuffer(ID3D11DeviceContext *pDeviceContext);
	void Load(ID3D11Device *pDevice, const char *fileName);
	void Draw(ID3D11DeviceContext *pDeviceContext, XMMATRIX *projection);
	void SetShader(TextureShader *pShader);
};