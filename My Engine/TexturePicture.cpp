#include "TexturePicture.h"

TexturePicture::TexturePicture()
{

}

void TexturePicture::SetPosition(float x, float y)
{
	Position.x = x;
	Position.y = y;
}

void TexturePicture::SetSize(unsigned long width, unsigned long height)
{
	this->Width = width;
	this->Height = height;
}

void TexturePicture::InitializeBuffers(ID3D11Device *pDevice)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA sd;

	unsigned long indices[] =
	{
		0, 1, 2,
		3, 4, 5
	};

	memset(&bd, 0, sizeof(bd));
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.ByteWidth = sizeof(Vertex) * 6;
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	pDevice->CreateBuffer(&bd, NULL, &pVertexBuffer);

	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.ByteWidth = sizeof(unsigned long) * 6;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.CPUAccessFlags = 0;

	memset(&sd, 0, sizeof(sd));
	sd.pSysMem = indices;
	pDevice->CreateBuffer(&bd, &sd, &pIndexBuffer);
}

void TexturePicture::UpdateVertexBuffer(ID3D11DeviceContext *pDeviceContext)
{
	float left = (float)Width / 2 * -1 + Position.x;
	float right = (float)Width / 2 + Position.x;
	float top = (float)Height / 2 - Position.y;
	float bottom = (float)Height / 2 * -1 - Position.y;

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	pDeviceContext->Map(pVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	Vertex *vertices = (Vertex *)mappedResource.pData;

	vertices[0].Position = XMFLOAT3(left, top, 0.0f);
	vertices[0].Tex = XMFLOAT2(0.0f, 0.0f);

	vertices[1].Position = XMFLOAT3(right, bottom, 0.0f);
	vertices[1].Tex = XMFLOAT2(1.0f, 1.0f);

	vertices[2].Position = XMFLOAT3(left, bottom, 0.0f);
	vertices[2].Tex = XMFLOAT2(0.0f, 1.0f);

	vertices[3].Position = XMFLOAT3(left, top, 0.0f);
	vertices[3].Tex = XMFLOAT2(0.0f, 0.0f);

	vertices[4].Position = XMFLOAT3(right, top, 0.0f);
	vertices[4].Tex = XMFLOAT2(1.0f, 0.0f);

	vertices[5].Position = XMFLOAT3(right, bottom, 0.0f);
	vertices[5].Tex = XMFLOAT2(1.0f, 1.0f);

	pDeviceContext->Unmap(pVertexBuffer, 0);
}

void TexturePicture::Load(ID3D11Device *pDevice, const char *fileName)
{
	D3DX11CreateShaderResourceViewFromFile(pDevice, fileName, NULL, NULL, &pTexture, NULL);
}

void TexturePicture::Draw(ID3D11DeviceContext *pDeviceContext, XMMATRIX *projection)
{
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	pShader->SetParameters(pDeviceContext, projection, pTexture);

	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pDeviceContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);
	pDeviceContext->IASetIndexBuffer(pIndexBuffer, DXGI_FORMAT_R32_UINT, offset);
	pDeviceContext->DrawIndexed(6, 0, 0);
}

void TexturePicture::SetShader(TextureShader *pShader)
{
	this->pShader = pShader;
}