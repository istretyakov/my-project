#pragma once

#include "Shader.h"

class FontShader : public Shader
{
public:
	struct ConstantBuffer
	{
		XMMATRIX WVP;
	};

private:
	ID3D11SamplerState *pSamplerState;
	ID3D11Buffer *pConstantBuffer;

public:
	int Create(ID3D11Device *pDevice, const char *fileVSName, const char *funcVS,
		const char *filePSName, const char *funcPS) override;
	int CreateOptions(ID3D11Device *pDevice);
	int SetParameters(ID3D11DeviceContext *pDeviceContext) override;
	int UpdateConstantBuffer(ID3D11DeviceContext *pDeviceContext, XMMATRIX *wvp);
};