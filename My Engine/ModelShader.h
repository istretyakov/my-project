#pragma once

#include "Shader.h"
#include "DirectionalLight.h"

class ModelShader : public Shader
{
public:
	struct ConstantBuffer
	{
		XMMATRIX WVP;
		XMMATRIX World;
	};

	struct LightBuffer
	{
		XMFLOAT4 l1;
		XMFLOAT4 ldc1;
		XMFLOAT4 lac1;
	};

private:
	ID3D11Buffer *pConstantBuffer;
	ID3D11Buffer *pLightBuffer;

public:
	int Create(ID3D11Device *pDevice, const char *fileVSName, const char *funcVS,
		const char *filePSName, const char *funcPS) override;
	void CreateConstantBuffer(ID3D11Device *pDevice);
	void UpdateConstantBuffer(ID3D11DeviceContext *pDeviceContext,
		XMMATRIX *world, XMMATRIX *view, XMMATRIX *projection);
	void SetParameters(ID3D11DeviceContext *pDeviceContext,
		XMMATRIX *world, XMMATRIX *view, XMMATRIX *projection);
	void SetLight(ID3D11DeviceContext *pDeviceContext, unsigned int numLights, DirectionalLight *lights);
};