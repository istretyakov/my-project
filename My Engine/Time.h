#pragma once

#include "stdafx.h"

class Time
{
private:
	INT64 PrevTime;
	float TicksPerMs;
	float FrameTime;

public:
	bool Initialize();
	void Frame();
	float GetTime();
};