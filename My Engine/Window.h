#pragma once

#include "stdafx.h"

#include "InputData.h"

struct WindowDesc
{
	int width;
	int height;
};

class Window
{
private:
	HWND hWnd;
	WindowDesc Desc;
	InputData InputInfo;

	static Window *thisWnd;

public:
	Window();

	static Window *Get();
	HWND *GetHWND();
	WindowDesc *GetDesc();
	InputData *GetInput();
	bool Initialize();
	bool CreateAndShow(int width, int height);
	bool Run();

	static LRESULT WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
};

LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);