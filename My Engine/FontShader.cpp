#include "FontShader.h"

int FontShader::Create(ID3D11Device *pDevice, const char *fileVSName, const char *funcVS,
	const char *filePSName, const char *funcPS)
{
	Shader::Create(pDevice, fileVSName, funcVS, filePSName, funcPS);

	CreateOptions(pDevice);

	return 0;
}

int FontShader::CreateOptions(ID3D11Device *pDevice)
{
	D3D11_BUFFER_DESC bd;
	D3D11_SAMPLER_DESC sd;

	memset(&bd, 0, sizeof(bd));
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.Usage = D3D11_USAGE_DEFAULT;

	pDevice->CreateBuffer(&bd, NULL, &pConstantBuffer);

	memset(&sd, 0, sizeof(sd));
	sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sd.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sd.MaxAnisotropy = 1;
	sd.MipLODBias = 0.0f;
	sd.MinLOD = 0.0f;
	sd.MaxLOD = D3D11_FLOAT32_MAX;

	pDevice->CreateSamplerState(&sd, &pSamplerState);

	return 0;
}

int FontShader::SetParameters(ID3D11DeviceContext *pDeviceContext)
{
	Shader::SetParameters(pDeviceContext);

	pDeviceContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
	pDeviceContext->PSSetSamplers(0, 1, &pSamplerState);

	return 0;
}

int FontShader::UpdateConstantBuffer(ID3D11DeviceContext *pDeviceContext, XMMATRIX *wvp)
{
	ConstantBuffer cb;

	cb.WVP = XMMatrixTranspose(*wvp);

	pDeviceContext->UpdateSubresource(pConstantBuffer, 0, NULL, &cb, 0, 0);

	return 0;
}