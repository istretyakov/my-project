#pragma once

#include "Camera.h"

class CameraThird : public Camera
{
private:
	float Radius;

public:
	CameraThird();

	void SetRadius(float radius);
	XMFLOAT3 Translate(XMFLOAT3 movement) override;
	void Rotate(XMFLOAT3 rotation) override;
	void GetViewMatrix(XMMATRIX &view) override;
};