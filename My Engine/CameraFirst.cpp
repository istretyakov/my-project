#include "CameraFirst.h"

XMFLOAT3 CameraFirst::Translate(XMFLOAT3 movement)
{
	float pitch = Rotation.x * 0.0174532925f;
	float yaw = Rotation.y * 0.0174532925f;
	float roll = Rotation.z * 0.0174532925f;

	XMMATRIX rotationMatrix = XMMatrixRotationRollPitchYaw(pitch, yaw, roll);

	XMVECTOR movVector = XMLoadFloat3(&movement);
	movVector = XMVector3TransformCoord(movVector, rotationMatrix);

	XMStoreFloat3(&movement, movVector);

	Position.x += movement.x;
	Position.y += movement.y;
	Position.z += movement.z;

	return movement;
}

void CameraFirst::Rotate(XMFLOAT3 rotation)
{
	Rotation.x -= rotation.y;

	if (Rotation.x > 70.0f)
		Rotation.x = 70.0f;
	else if (Rotation.x < -70.0f)
		Rotation.x = -70.0f;

	Rotation.y -= rotation.x;

	if (Rotation.y < 0.0f || Rotation.y >= 360.0f)
		Rotation.y -= (int)(Rotation.y / 360) * 360;
}

void CameraFirst::GetViewMatrix(XMMATRIX &view)
{
	XMVECTOR eye = XMVectorSet(Position.x, Position.y, Position.z, 0.0f);
	XMVECTOR at = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	float pitch = Rotation.x * 0.0174532925f;
	float yaw = Rotation.y * 0.0174532925f;
	float roll = Rotation.z * 0.0174532925f;

	XMMATRIX rotationMatrix = XMMatrixRotationRollPitchYaw(pitch, yaw, roll);

	at = XMVector3TransformCoord(at, rotationMatrix);
	up = XMVector3TransformCoord(up, rotationMatrix);

	at = eye + at;

	view = XMMatrixLookAtLH(eye, at, up);
}