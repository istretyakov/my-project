#include "Camera.h"

Camera::Camera()
{
	Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Rotation = XMFLOAT3(0.0f, 0.0f, 0.0f);
}

void Camera::SetPosition(float x, float y, float z)
{
	Position.x = x;
	Position.y = y;
	Position.z = z;
}

void Camera::GetPosition(float &x, float &y, float &z)
{
	x = Position.x;
	y = Position.y;
	z = Position.z;
}

void Camera::SetRotation(float x, float y, float z)
{
	Rotation.x = x;
	Rotation.y = y;
	Rotation.z = z;
}

void Camera::GetRotation(float &x, float &y, float &z)
{
	x = Rotation.x;
	y = Rotation.y;
	z = Rotation.z;
}