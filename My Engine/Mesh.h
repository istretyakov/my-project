#pragma once

#include "stdafx.h"

class Mesh
{
public:
	struct Vertex
	{
		XMFLOAT3 Position;
		XMFLOAT3 Normal;
	};

	struct ConstantBuffer
	{
		XMMATRIX WVP;
	};

private:
	ID3D11InputLayout *pInputLayout;
	ID3D11VertexShader *pVertexShader;
	ID3D11PixelShader *pPixelShader;
	ID3D11Buffer *pVertexBuffer;
	ID3D11Buffer *pIndexBuffer;
	Vertex *Vertices;
	unsigned int *Indices;
	unsigned int VertexCount;
	unsigned int IndexCount;

public:
	void Draw(ID3D11DeviceContext *pDeviceContext);
	void CreateBuffers(ID3D11Device *pDevice);
	void LoadFromFile(const char *fileName);
	void ParseCount(const char *fileName, unsigned int &numVertices, unsigned int &numNormals, unsigned int &numFaces);
	void ParseData(const char *fileName, unsigned int numVertices, unsigned int numNormals, unsigned int numFaces);
};