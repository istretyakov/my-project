#include "Framework.h"

Framework::Framework()
{
	Wnd = nullptr;
	Rnd = nullptr;
}

int Framework::Initialize(int width, int height)
{
	bool result = false;
	
	Wnd = new Window;

	result = Wnd->Initialize();
	if (result)
		return 1;
	result = Wnd->CreateAndShow(width, height);
	if (result)
		return 2;

	if (Rnd != nullptr)
	{
		Rnd->SetWindow(Wnd);
		Rnd->InitializeDirectX();
		Rnd->InitializeScene();
	}

	return 0;
}

void Framework::SetRender(Render *render)
{
	Rnd = render;
}

void Framework::Run()
{
	bool open = true;
	while (open)
	{
		open = Wnd->Run();

		if (open)
		{
			Rnd->Draw();
		}
	}
}

void Framework::Close()
{
	if (Wnd)
	{
		delete Wnd;
		Wnd = nullptr;
	}

	if (Rnd)
		Rnd = nullptr;
}