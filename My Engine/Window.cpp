#include "Window.h"

Window * Window::thisWnd = nullptr;

Window::Window()
{
	thisWnd = this;
}

Window * Window::Get()
{
	return thisWnd;
}

HWND * Window::GetHWND()
{
	return &hWnd;
}

WindowDesc * Window::GetDesc()
{
	return &Desc;
}

InputData * Window::GetInput()
{
	return &InputInfo;
}

bool Window::Initialize()
{
	WNDCLASSEX wcex;
	ZeroMemory(&wcex, sizeof(wcex));
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = StaticWndProc;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wcex.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wcex.lpszClassName = "Test";

	HRESULT hr = RegisterClassEx(&wcex);
	if (!hr)			// ���������� true, ���� ����� ���� �� ��� ������
		return true;

	return false;
}

bool Window::CreateAndShow(int width, int height)
{
	Desc.width = width;
	Desc.height = height;

	hWnd = CreateWindow("Test", "Test", WS_OVERLAPPEDWINDOW, 200, 200, Desc.width, Desc.height, NULL, (HMENU)NULL, NULL, NULL);
	if (!hWnd)			// ���������� true, ���� ���� �� ���� �������
		return true;

	ShowWindow(hWnd, SW_SHOW);

	return false;
}

LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	return Window::WndProc(hWnd, Msg, wParam, lParam);
}

LRESULT Window::WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	switch (Msg)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		break;
	}
	case WM_LBUTTONUP: case WM_LBUTTONDOWN: case WM_MBUTTONUP: case WM_MBUTTONDOWN:
	case WM_RBUTTONUP: case WM_RBUTTONDOWN: case WM_MOUSEWHEEL: case WM_MOUSEMOVE:
	case WM_KEYUP: case WM_KEYDOWN:
	{
		Window::Get()->InputInfo.PutInputData(Msg, wParam, lParam);
		break;
	}
	case WM_SIZE:
	{
		break;
	}
	default: return DefWindowProc(hWnd, Msg, wParam, lParam);
	}

	return 0;
}

bool Window::Run()
{
	MSG msg;

	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		if (msg.message == WM_QUIT)
			return false;
	}

	return true;
}