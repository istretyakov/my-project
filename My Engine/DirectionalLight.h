#pragma once

#include "stdafx.h"

class DirectionalLight
{
private:
	XMFLOAT4 Diffuse;
	XMFLOAT4 Ambient;
	XMFLOAT3 Direction;

public:
	DirectionalLight();

	void SetDirection(float x, float y, float z);
	void GetDirection(float &x, float &y, float &z);
	void SetDiffuse(float r, float g, float b);
	void GetDiffuse(float &r, float &g, float &b);
	void SetAmbient(float r, float g, float b);
	void GetAmbient(float &r, float &g, float &b);
};