#include "ObjectModel.h"

ObjectModel::ObjectModel()
{
	pShader = nullptr;
	Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Rotation = XMFLOAT3(0.0f, 0.0f, 0.0f);
}

void ObjectModel::CreateMesh(ID3D11Device *pDevice, const char *fileName)
{
	ModelMesh.LoadFromFile(fileName);
	ModelMesh.CreateBuffers(pDevice);
}

void ObjectModel::SetShader(ModelShader *pShader)
{
	this->pShader = pShader;
}

void ObjectModel::GetWorldMatrix(XMMATRIX &world)
{
	XMMATRIX rotation = XMMatrixRotationRollPitchYaw(Rotation.x * 3.14f / 180, Rotation.y * 3.14f / 180, Rotation.z * 3.14f / 180);
	XMMATRIX position = XMMatrixTranslation(Position.x, Position.y, Position.z);

	world = rotation * position;
}

void ObjectModel::SetPosition(float x, float y, float z)
{
	Position = XMFLOAT3(x, y, z);
}

void ObjectModel::GetPosition(float &x, float &y, float &z)
{
	x = Position.x;
	y = Position.y;
	z = Position.z;
}

void ObjectModel::SetRotation(float x, float y, float z)
{
	Rotation = XMFLOAT3(x, y, z);
}

void ObjectModel::GetRotation(float &x, float &y, float &z)
{
	x = Rotation.x;
	y = Rotation.y;
	z = Rotation.z;
}

void ObjectModel::Draw(ID3D11DeviceContext *pDeviceContext, XMMATRIX *view, XMMATRIX *projection)
{
	XMMATRIX world;
	GetWorldMatrix(world);

	pShader->SetParameters(pDeviceContext, &world, view, projection);
	ModelMesh.Draw(pDeviceContext);
}