#pragma once

#include "stdafx.h"
#include <map>
#include <cstring>

#include "FontShader.h"

class Font
{
public:
	struct Vertex
	{
		XMFLOAT3 Position;
		XMFLOAT2 Tex;
	};

public:
	struct CharDesc
	{
		CharDesc() : x(0), y(0), width(0), height(0), xoffset(0), yoffset(0), xadvance(0)
		{ }

		short x;
		short y;
		short width;
		short height;
		short xoffset;
		short yoffset;
		short xadvance;
	};

private:
	FontShader *pShader;
	ID3D11ShaderResourceView *pTexture;

	std::map<int, CharDesc> Chars;

public:
	int Create(ID3D11Device *pDevice, const char *fontFile, const char *fileVSName, const char *funcVS,
		const char *filePSName, const char *funcPS);
	int ParseData(ID3D11Device *pDevice, const char *fileName, std::string *textureOut);
	int Load(ID3D11Device *pDevice, const char *fileName);
	int BuildVertexBuffer(ID3D11DeviceContext *pDeviceContext, ID3D11Buffer *pVertexBuffer,
		const char *text, float width, float height);
	int SetParameters(ID3D11DeviceContext *pDeviceContext, XMMATRIX *wvp);
};