#include "TextureShader.h"

int TextureShader::Create(ID3D11Device *pDevice, const char *fileVSName, const char *funcVS,
	const char *filePSName, const char *funcPS)
{
	Shader::Create(pDevice, fileVSName, funcVS, filePSName, funcPS);

	CreateConstantBuffer(pDevice);

	D3D11_SAMPLER_DESC sd;
	memset(&sd, 0, sizeof(sd));
	sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sd.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sd.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sd.MaxAnisotropy = 1;
	sd.MipLODBias = 0.0f;
	sd.MinLOD = 0.0f;
	sd.MaxLOD = D3D11_FLOAT32_MAX;

	pDevice->CreateSamplerState(&sd, &pSamplerLinear);

	return 0;
}

void TextureShader::CreateConstantBuffer(ID3D11Device *pDevice)
{
	D3D11_BUFFER_DESC bd;
	memset(&bd, 0, sizeof(bd));

	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	pDevice->CreateBuffer(&bd, NULL, &pConstantBuffer);
}

void TextureShader::UpdateConstantBuffer(ID3D11DeviceContext *pDeviceContext, XMMATRIX *projection)
{
	ConstantBuffer cb;

	cb.Ortho = XMMatrixTranspose(*projection);

	pDeviceContext->UpdateSubresource(pConstantBuffer, 0, NULL, &cb, 0, 0);
}

void TextureShader::SetParameters(ID3D11DeviceContext *pDeviceContext, XMMATRIX *projection,
	ID3D11ShaderResourceView *picture)
{
	UpdateConstantBuffer(pDeviceContext, projection);

	Shader::SetParameters(pDeviceContext);

	pDeviceContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
	pDeviceContext->PSSetShaderResources(0, 1, &picture);
	pDeviceContext->PSSetSamplers(0, 1, &pSamplerLinear);
}