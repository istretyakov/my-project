#include "Font.h"

#include <sstream>
#include <fstream>
#include <iostream>

int Font::Create(ID3D11Device *pDevice, const char *fontFile, const char *fileVSName, const char *funcVS,
	const char *filePSName, const char *funcPS)
{
	std::string textureName;

	ParseData(pDevice, fontFile, &textureName);

	pShader = new FontShader;
	pShader->AddInputElement("POSITION", DXGI_FORMAT_R32G32B32_FLOAT);
	pShader->AddInputElement("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT);
	pShader->Create(pDevice, fileVSName, funcVS, filePSName, funcPS);

	Load(pDevice, textureName.c_str());

	return 0;
}

int Font::ParseData(ID3D11Device *pDevice, const char *fileName, std::string *textureOut)
{
	std::ifstream infile;

	infile.open(fileName);

	if (infile.is_open())
	{
		while (!infile.eof())
		{
			std::string lineString;
			std::stringstream lineStream;
			std::string token;

			std::string key;
			std::string value;
			size_t pos;

			std::getline(infile, lineString);
			lineStream << lineString;

			lineStream >> token;

			if (token == "page")
			{
				while (!lineStream.eof())
				{
					std::stringstream converter;
					lineStream >> token;
					pos = token.find("=");
					key = token.substr(0, pos);
					value = token.substr(pos + 1);
					converter << value;

					if (key == "file")
					{
						std::string name;
						converter >> name;
						name = name.substr(1, name.size() - 2);

						*textureOut = name;
					}
				}
			}
			else if (token == "char")
			{
				int id = 0;
				CharDesc newChar;

				while (!lineStream.eof())
				{
					std::stringstream converter;
					lineStream >> token;
					pos = token.find("=");
					key = token.substr(0, pos);
					value = token.substr(pos + 1);
					converter << value;

					if (key == "id")
						converter >> id;
					else if (key == "x")
						converter >> newChar.x;
					else if (key == "y")
						converter >> newChar.y;
					else if (key == "width")
						converter >> newChar.width;
					else if (key == "height")
						converter >> newChar.height;
					else if (key == "xoffset")
						converter >> newChar.xoffset;
					else if (key == "yoffset")
						converter >> newChar.yoffset;
					else if (key == "xadvance")
						converter >> newChar.xadvance;
				}

				Chars.insert(std::pair<int, CharDesc>(id, newChar));
			}
		}

		infile.close();
	}
	else
		return 1;

	return 0;
}

int Font::Load(ID3D11Device *pDevice, const char *fileName)
{
	D3DX11CreateShaderResourceViewFromFile(pDevice, fileName, NULL, NULL, &pTexture, NULL);

	return 0;
}

int Font::BuildVertexBuffer(ID3D11DeviceContext *pDeviceContext, ID3D11Buffer *pVertexBuffer,
	const char *text, float width, float height)
{
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	float posX = -(float)width / 2, posY = (float)height / 2;

	pDeviceContext->Map(pVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);

	Vertex *vertices = (Vertex *)mappedSubresource.pData;

	unsigned int index = 0;

	for (int i = 0; i < strlen(text); i++)
	{
		float left = posX + Chars[text[i]].xoffset;
		float top = posY - Chars[text[i]].yoffset;
		float right = left + Chars[text[i]].width;
		float bottom = top - Chars[text[i]].height;

		float leftTex = Chars[text[i]].x / 512.0f;
		float topTex = Chars[text[i]].y / 512.0f;
		float rightTex = leftTex + Chars[text[i]].width / 512.0f;
		float bottomTex = topTex + Chars[text[i]].height / 512.0f;

		posX += Chars[text[i]].xadvance;

		vertices[index].Position = XMFLOAT3(left, top, 0.0f);
		vertices[index].Tex = XMFLOAT2(leftTex, topTex);
		index++;

		vertices[index].Position = XMFLOAT3(right, bottom, 0.0f);
		vertices[index].Tex = XMFLOAT2(rightTex, bottomTex);
		index++;

		vertices[index].Position = XMFLOAT3(left, bottom, 0.0f);
		vertices[index].Tex = XMFLOAT2(leftTex, bottomTex);
		index++;

		vertices[index].Position = XMFLOAT3(left, top, 0.0f);
		vertices[index].Tex = XMFLOAT2(leftTex, topTex);
		index++;

		vertices[index].Position = XMFLOAT3(right, top, 0.0f);
		vertices[index].Tex = XMFLOAT2(rightTex, topTex);
		index++;

		vertices[index].Position = XMFLOAT3(right, bottom, 0.0f);
		vertices[index].Tex = XMFLOAT2(rightTex, bottomTex);
		index++;
	}

	pDeviceContext->Unmap(pVertexBuffer, 0);

	return 0;
}

int Font::SetParameters(ID3D11DeviceContext *pDeviceContext, XMMATRIX *wvp)
{
	pShader->UpdateConstantBuffer(pDeviceContext, wvp);
	pShader->SetParameters(pDeviceContext);

	pDeviceContext->PSSetShaderResources(0, 1, &pTexture);

	return 0;
}