#include "Time.h"

bool Time::Initialize()
{
	INT64 frequency;

	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);

	if (frequency == 0)
		return false;

	TicksPerMs = (float)(frequency / 1000);

	QueryPerformanceCounter((LARGE_INTEGER *)PrevTime);

	return true;
}

void Time::Frame()
{
	INT64 currentTime;
	float timeDifference;

	QueryPerformanceFrequency((LARGE_INTEGER *)&currentTime);

	timeDifference = (float)(currentTime - PrevTime);

	FrameTime = timeDifference / TicksPerMs;

	PrevTime = currentTime;
}

float Time::GetTime()
{
	return FrameTime;
}