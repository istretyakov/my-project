#pragma once

#include "Shader.h"

class TextureShader : public Shader
{
public:
	struct ConstantBuffer
	{
		XMMATRIX Ortho;
	};

private:
	ID3D11Buffer *pConstantBuffer;

	ID3D11SamplerState *pSamplerLinear;

public:
	int Create(ID3D11Device *pDevice, const char *fileVSName, const char *funcVS,
		const char *filePSName, const char *funcPS) override;
	void CreateConstantBuffer(ID3D11Device *pDevice);
	void UpdateConstantBuffer(ID3D11DeviceContext *pDeviceContext, XMMATRIX *projection);
	void SetParameters(ID3D11DeviceContext *pDeviceContext, XMMATRIX *projection,
		ID3D11ShaderResourceView *picture);
};