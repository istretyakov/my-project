#pragma once

#include "stdafx.h"

#include "Font.h"

class Text
{
public:
	struct Vertex
	{
		XMFLOAT3 Position;
		XMFLOAT2 Tex;
	};

private:
	ID3D11Buffer *pVertexBuffer;
	ID3D11Buffer *pIndexBuffer;

	unsigned int NumIndices;
	unsigned int NumSymbols;		// ������������ ���������� ��������
	char *Content;					// ������ � NumSymbols ����������� ��������, �� ��������
	XMFLOAT2 Position;
	// XMFLOAT
	Font *pFont;

public:
	Text();

	void SetFont(Font *font);
	int SetText(ID3D11DeviceContext *pDeviceContext, const char *text);
	int UpdateVertexBuffer(ID3D11DeviceContext *pDeviceContext);
	int Create(ID3D11Device *pDevice, ID3D11DeviceContext *pDeviceContext, const char *text, int numSymbols);
	int InitializeBuffer(ID3D11Device *pDevice);
	int Draw(ID3D11DeviceContext *pDeviceContext, XMMATRIX *projection);
};