#include "Render.h"

Render::Render()
{

}

int Render::InitializeDirectX()
{
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0
	};

	D3D_FEATURE_LEVEL featureLevel;

	DXGI_SWAP_CHAIN_DESC scd;
	memset(&scd, 0, sizeof(scd));
	scd.BufferCount = 1;
	scd.BufferDesc.Width = Wnd->GetDesc()->width;
	scd.BufferDesc.Height = Wnd->GetDesc()->height;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 1;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.OutputWindow = *Wnd->GetHWND();
	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;
	scd.Windowed = TRUE;

	D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, featureLevels, 1, D3D11_SDK_VERSION, &scd, &pSwapChain, &pDevice, &featureLevel, &pDeviceContext);

	ID3D11Texture2D *backBuffer;
	pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void **)&backBuffer);
	pDevice->CreateRenderTargetView(backBuffer, NULL, &pRenderTargetView);

	D3D11_TEXTURE2D_DESC dtd;
	memset(&dtd, 0, sizeof(dtd));
	dtd.Width = Wnd->GetDesc()->width;
	dtd.Height = Wnd->GetDesc()->height;
	dtd.MipLevels = 1;
	dtd.ArraySize = 1;
	dtd.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dtd.SampleDesc.Count = 1;
	dtd.SampleDesc.Quality = 0;
	dtd.Usage = D3D11_USAGE_DEFAULT;
	dtd.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	dtd.CPUAccessFlags = 0;
	dtd.MiscFlags = 0;
	pDevice->CreateTexture2D(&dtd, NULL, &pDepthStencil);

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	memset(&dsvd, 0, sizeof(dsvd));
	dsvd.Format = dtd.Format;
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvd.Texture2D.MipSlice = 0;
	pDevice->CreateDepthStencilView(pDepthStencil, &dsvd, &pDepthStencilView);

	D3D11_DEPTH_STENCIL_DESC dsd;
	memset(&dsd, 0, sizeof(dsd));
	dsd.DepthEnable = true;
	dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsd.DepthFunc = D3D11_COMPARISON_LESS;
	dsd.StencilEnable = true;
	dsd.StencilReadMask = 0xFF;
	dsd.StencilWriteMask = 0xFF;
	dsd.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsd.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsd.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsd.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsd.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsd.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsd.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsd.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	pDevice->CreateDepthStencilState(&dsd, &pDepthStencilState);

	dsd.DepthEnable = false;
	pDevice->CreateDepthStencilState(&dsd, &pDepthDisabledStencilState);

	pDeviceContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);

	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)Wnd->GetDesc()->width;
	vp.Height = (FLOAT)Wnd->GetDesc()->height;
	vp.MinDepth = 0.0;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	pDeviceContext->RSSetViewports(1, &vp);

	return 0;
}

void Render::SetWindow(Window *window)
{
	Wnd = window;
}

void Render::Draw()
{
	BeginDraw();

	DrawScene();

	EndDraw();
}

void Render::BeginDraw()
{
	float color[] = { 0.69f, 0.86f, 0.99f, 1.0f };
	TurnZBufferOn();
	pDeviceContext->ClearRenderTargetView(pRenderTargetView, color);
	pDeviceContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void Render::EndDraw()
{
	pSwapChain->Present(0, 0);
}

void Render::TurnZBufferOn()
{
	pDeviceContext->OMSetDepthStencilState(pDepthStencilState, 1);
}

void Render::TurnZBufferOff()
{
	pDeviceContext->OMSetDepthStencilState(pDepthDisabledStencilState, 1);
}