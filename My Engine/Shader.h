#pragma once

#include "stdafx.h"

class Shader
{
protected:
	D3D11_INPUT_ELEMENT_DESC *Elements;
	unsigned int NumElements;
	ID3D11InputLayout *pInputLayout;
	ID3D11VertexShader *pVertexShader;
	ID3D11PixelShader *pPixelShader;

public:
	Shader();

	virtual int Create(ID3D11Device *pDevice, const char *fileVSName, const char *funcVS,
		const char *filePSName, const char *funcPS);
	int AddInputElement(const char *elemName, DXGI_FORMAT format);
	virtual int SetParameters(ID3D11DeviceContext *pDeviceContext);
};