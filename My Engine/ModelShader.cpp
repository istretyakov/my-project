#include "ModelShader.h"

int ModelShader::Create(ID3D11Device *pDevice, const char *fileVSName, const char *funcVS,
	const char *filePSName, const char *funcPS)
{
	Shader::Create(pDevice, fileVSName, funcVS, filePSName, funcPS);

	CreateConstantBuffer(pDevice);

	return 0;
}

void ModelShader::CreateConstantBuffer(ID3D11Device *pDevice)
{
	D3D11_BUFFER_DESC bd;
	memset(&bd, 0, sizeof(bd));

	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	pDevice->CreateBuffer(&bd, NULL, &pConstantBuffer);

	bd.ByteWidth = sizeof(LightBuffer);

	pDevice->CreateBuffer(&bd, NULL, &pLightBuffer);
}

void ModelShader::UpdateConstantBuffer(ID3D11DeviceContext *pDeviceContext,
	XMMATRIX *world, XMMATRIX *view, XMMATRIX *projection)
{
	ConstantBuffer cb;

	cb.WVP = XMMatrixTranspose((*world) * (*view) * (*projection));
	cb.World = XMMatrixTranspose(*world);

	pDeviceContext->UpdateSubresource(pConstantBuffer, 0, NULL, &cb, 0, 0);
}

void ModelShader::SetParameters(ID3D11DeviceContext *pDeviceContext,
	XMMATRIX *world, XMMATRIX *view, XMMATRIX *projection)
{
	UpdateConstantBuffer(pDeviceContext, world, view, projection);

	pDeviceContext->IASetInputLayout(pInputLayout);
	pDeviceContext->VSSetShader(pVertexShader, NULL, 0);
	pDeviceContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
	pDeviceContext->PSSetShader(pPixelShader, NULL, 0);
	pDeviceContext->PSSetConstantBuffers(0, 1, &pLightBuffer);
}

void ModelShader::SetLight(ID3D11DeviceContext *pDeviceContext, unsigned int numLights, DirectionalLight *lights)
{
	float x, y, z;
	float dr, dg, db;
	float ar, ag, ab;

	LightBuffer lb;

	lights[0].GetDirection(x, y, z);
	lights[0].GetDiffuse(dr, dg, db);
	lights[0].GetAmbient(ar, ag, ab);

	lb.l1 = XMFLOAT4(x, y, z, 0.0f);
	lb.ldc1 = XMFLOAT4(dr, dg, db, 1.0f);
	lb.lac1 = XMFLOAT4(ar, ag, ab, 1.0f);

	pDeviceContext->UpdateSubresource(pLightBuffer, 0, NULL, &lb, 0, 0);
}