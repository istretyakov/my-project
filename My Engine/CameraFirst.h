#pragma once

#include "Camera.h"

class CameraFirst : public Camera
{
public:
	XMFLOAT3 Translate(XMFLOAT3 movement) override;
	void Rotate(XMFLOAT3 rotation) override;
	void GetViewMatrix(XMMATRIX &view) override;
};