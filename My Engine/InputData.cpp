#include "InputData.h"

#include <iostream>

InputData::InputData()
{
	for (int i = 0; i < MAX_KEYS; i++)
		Keys[i] = false;

	for (int i = 0; i < MAX_MOUSE_KEYS; i++)
		MouseKeys[i] = false;

	MousePosition.x = 0;
	MousePosition.y = 0;
}

void InputData::PutInputData(UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_LBUTTONUP:
	{
		SetMouseKeyState(MOUSE_LEFT, false);
		break;
	}
	case WM_LBUTTONDOWN:
	{
		SetMouseKeyState(MOUSE_LEFT, true);
		break;
	}
	case WM_RBUTTONUP:
	{
		SetMouseKeyState(MOUSE_RIGHT, false);
		break;
	}
	case WM_RBUTTONDOWN:
	{
		SetMouseKeyState(MOUSE_RIGHT, true);
		break;
	}
	case WM_MBUTTONUP:
	{
		SetMouseKeyState(MOUSE_MIDDLE, false);
		break;
	}
	case WM_MBUTTONDOWN:
	{
		SetMouseKeyState(MOUSE_MIDDLE, true);
		break;
	}
	case WM_KEYUP:
	{
		SetKeyState((int)wParam, false);
		break;
	}
	case WM_KEYDOWN:
	{
		SetKeyState((int)wParam, true);
		break;
	}
	case WM_MOUSEMOVE:
	{
		SetMousePosition((int)LOWORD(lParam), (int)HIWORD(lParam));
		break;
	}
	}
}

void InputData::SetKeyState(int key, bool state)
{
	if (key >= 0 && key < MAX_KEYS)
		Keys[key] = state;
}

void InputData::SetMouseKeyState(int key, bool state)
{
	if (key >= 0 && key < MAX_MOUSE_KEYS)
		MouseKeys[key] = state;
}

void InputData::SetMousePosition(int x, int y)
{
	MousePosition.x = x;
	MousePosition.y = y;
}

void InputData::GetMousePosition(int &x, int &y)
{
	x = MousePosition.x;
	y = MousePosition.y;
}

bool InputData::IsKeyDown(int key)
{
	if (key >= 0 && key < MAX_KEYS)
	{
		if (Keys[key])
			return true;
	}

	return false;
}

bool InputData::IsMouseKeyDown(int key)
{
	if (key >= 0 && key < MAX_MOUSE_KEYS)
	{
		if (MouseKeys[key])
			return true;
	}

	return false;
}