#pragma once

#include "stdafx.h"
#include "Window.h"

class Render
{
protected:
	ID3D11Device *pDevice;
	IDXGISwapChain *pSwapChain;
	ID3D11DeviceContext *pDeviceContext;
	ID3D11RenderTargetView *pRenderTargetView;
	ID3D11Texture2D *pDepthStencil;
	ID3D11DepthStencilView *pDepthStencilView;
	ID3D11DepthStencilState *pDepthStencilState;
	ID3D11DepthStencilState *pDepthDisabledStencilState;

	Window *Wnd;

	void BeginDraw();
	virtual void DrawScene() = 0;
	void EndDraw();

public:
	Render();

	int InitializeDirectX();
	virtual void InitializeScene() = 0;
	void SetWindow(Window *window);
	void Draw();
	void TurnZBufferOn();
	void TurnZBufferOff();
};