#include "PointLight.h"

PointLight::PointLight()
{

}

void PointLight::SetRadius(long radius)
{
	Radius = radius;
}

long PointLight::GetRadius()
{
	return Radius;
}

void PointLight::SetDiffuse(float r, float g, float b)
{
	Diffuse.x = r;
	Diffuse.y = g;
	Diffuse.z = b;
}

void PointLight::GetDiffuse(float &r, float &g, float &b)
{
	r = Diffuse.x;
	g = Diffuse.y;
	b = Diffuse.z;
}

void PointLight::SetAmbient(float r, float g, float b)
{
	Ambient.x = r;
	Ambient.y = g;
	Ambient.z = b;
}

void PointLight::GetAmbient(float &r, float &g, float &b)
{
	r = Ambient.x;
	g = Ambient.y;
	b = Ambient.z;
}