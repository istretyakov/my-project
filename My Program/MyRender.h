#pragma once

#include "stdafx.h"

#include "Render.h"
#include "ObjectModel.h"
#include "ModelShader.h"
#include "TextureShader.h"
#include "TexturePicture.h"
#include "CameraFirst.h"
#include "CameraThird.h"
#include "FrameTimer.h"
#include "DirectionalLight.h"
#include "Font.h"
#include "Text.h"

class MyRender : public Render
{
private:
	ID3D11InputLayout *pInputLayout;
	ID3D11VertexShader *pVertexShader;
	ID3D11PixelShader *pPixelShader;
	ID3D11Buffer *pVertexBuffer;
	ID3D11Buffer *pIndexBuffer;
	ID3D11Buffer *pConstantBuffer;

	ID3D11BlendState *translucency;

	ModelShader *pShader;
	ObjectModel Model;
	ObjectModel Model2;
	ObjectModel Plane;

	CameraFirst Cam1;
	CameraThird Cam3;

	DirectionalLight l1;

	FrameTimer Time;

	TextureShader *pTShader;
	TexturePicture Picture1;
	TexturePicture Picture2;

	Font *pFont;
	Text Text1;

public:
	struct Vertex
	{
		XMFLOAT3 Position;
	};

	void InitializeScene() override;
	void DrawScene() override;
};