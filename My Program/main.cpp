#include "Framework.h"
#include "MyRender.h"

int main()
{
	Framework framework;
	MyRender render;

	framework.SetRender(&render);
	int result = framework.Initialize(1024, 720);
	if (result)
		return 1;

	framework.Run();
	framework.Close();

	int i = NULL;
	int *j = nullptr;

	return 0;
}