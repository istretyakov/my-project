#pragma once

#include "stdafx.h"

class PointLight
{
private:
	XMFLOAT4 Diffuse;
	XMFLOAT4 Ambient;
	long Radius;

public:
	PointLight();

	void SetRadius(long radius);
	long GetRadius();
	void SetDiffuse(float r, float g, float b);
	void GetDiffuse(float &r, float &g, float &b);
	void SetAmbient(float r, float g, float b);
	void GetAmbient(float &r, float &g, float &b);
};