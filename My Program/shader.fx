cbuffer ConstantBuffer
{
    matrix WVP;
};

struct PS_INPUT
{
    float4 Position : SV_POSITION;
};

PS_INPUT VS(float4 Pos : POSITION)
{
    PS_INPUT output;

    output.Position = mul(Pos, WVP);
    // output.Position = Pos;

    return output;
}

float4 PS(PS_INPUT input) : SV_Target
{
    return float4(1.0f, 1.0f, 0.0f, 1.0f);
}