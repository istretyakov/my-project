cbuffer ConstantBuffer
{
    matrix Ortho;
};

struct VS_INPUT
{
	float4 Position : POSITION;
	float2 Tex : TEXCOORD;
};

struct PS_INPUT
{
    float4 Position : SV_POSITION;
	float2 Tex : TEXCOORD;
};

PS_INPUT VS(VS_INPUT input)
{
    PS_INPUT output;

    output.Position = mul(input.Position, Ortho);
	output.Tex = input.Tex;

    return output;
}