#include "MyRender.h"

#include "InputCodes.h"

void MyRender::InitializeScene()
{
	Time.Initialize();

	Cam3.SetRadius(5.0f);
	Cam3.SetPosition(0.0f, 1.0f, 0.0f);

	pShader = new ModelShader;
	pShader->AddInputElement("POSITION", DXGI_FORMAT_R32G32B32_FLOAT);
	pShader->AddInputElement("NORMAL", DXGI_FORMAT_R32G32B32_FLOAT);
	pShader->Create(pDevice, "modelshader.vs", "VS", "modelshader.ps", "PS");

	pTShader = new TextureShader;
	pTShader->AddInputElement("POSITION", DXGI_FORMAT_R32G32B32_FLOAT);
	pTShader->AddInputElement("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT);
	pTShader->Create(pDevice, "textureshader.vs", "VS", "textureshader.ps", "PS");

	Model.SetShader(pShader);
	Model.CreateMesh(pDevice, "Cube.obj");
	Model.SetPosition(0.0f, 1.0f, 0.0f);

	Model2.SetShader(pShader);
	Model2.CreateMesh(pDevice, "cylinder.obj");
	Model2.SetPosition(0.0f, 1.0f, 0.0f);

	Plane.SetShader(pShader);
	Plane.CreateMesh(pDevice, "plane.obj");

	l1.SetDirection(0.0f, 1.0f, 0.0f);
	l1.SetDiffuse(0.0f, 0.0f, 0.5f);
	l1.SetAmbient(0.3f, 0.3f, 0.3f);

	pShader->SetLight(pDeviceContext, 1, &l1);

	Picture1.SetPosition(0, 0);
	Picture1.SetSize(40, 40);
	Picture1.Load(pDevice, "Texture.png");
	Picture1.InitializeBuffers(pDevice);
	Picture1.UpdateVertexBuffer(pDeviceContext);
	Picture1.SetShader(pTShader);

	Picture2.SetPosition(0, 0);
	Picture2.SetSize(40, 40);
	Picture2.Load(pDevice, "font_0.png");
	Picture2.InitializeBuffers(pDevice);
	Picture2.UpdateVertexBuffer(pDeviceContext);
	Picture2.SetShader(pTShader);

	pFont = new Font;
	pFont->Create(pDevice, "font.fnt", "fontshader.vs", "VS", "fontshader.ps", "PS");

	Text1.SetFont(pFont);
	Text1.Create(pDevice, pDeviceContext, "TN", 4);

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	memset(&rtbd, 0, sizeof(rtbd));
	rtbd.BlendEnable = TRUE;
	rtbd.SrcBlend = D3D11_BLEND_SRC_COLOR;
	rtbd.DestBlend = D3D11_BLEND_INV_DEST_COLOR;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ONE;
	rtbd.DestBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = 0x0f;

	D3D11_BLEND_DESC blendDesc;
	memset(&blendDesc, 0, sizeof(blendDesc));
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.RenderTarget[0] = rtbd;

	pDevice->CreateBlendState(&blendDesc, &translucency);
}

void MyRender::DrawScene()
{
	Time.Frame();
	float deltaTime = Time.GetDelta();

	if (Wnd->GetInput()->IsMouseKeyDown(MOUSE_LEFT))
	{
		static int x = 0, y = 0;
		int oldX = x, oldY = y;
		Wnd->GetInput()->GetMousePosition(x, y);
		int deltaX = x - oldX, deltaY = y - oldY;

		float rx, ry, rz;
		Model.GetRotation(rx, ry, rz);
		Model.SetRotation(rx + deltaY * deltaTime, ry + deltaX * deltaTime, rz);
	}

	XMFLOAT3 movement = XMFLOAT3(0.0f, 0.0f, 0.0f);

	if (Wnd->GetInput()->IsKeyDown(KEY_W))
		movement.z += 0.0025f;
	if (Wnd->GetInput()->IsKeyDown(KEY_S))
		movement.z -= 0.0025f;
	if (Wnd->GetInput()->IsKeyDown(KEY_A))
		movement.x -= 0.0025f;
	if (Wnd->GetInput()->IsKeyDown(KEY_D))
		movement.x += 0.0025f;

	movement.x *= deltaTime;
	movement.y *= deltaTime;
	movement.z *= deltaTime;

	int dx, dy;
	Wnd->GetInput()->GetMousePosition(dx, dy);

	static int pos_x = dx;
	static int pos_y = dy;

	int cur_x = dx;
	int cur_y = dy;

	int deltaX = pos_x - cur_x;
	int deltaY = pos_y - cur_y;

	pos_x = cur_x;
	pos_y = cur_y;

	Cam1.Rotate(XMFLOAT3(deltaX, deltaY, 0.0f));
	Cam3.Rotate(XMFLOAT3(deltaX, deltaY, 0.0f));

	XMMATRIX view;
	Cam1.Translate(movement);
	Cam1.GetViewMatrix(view);
	movement = Cam3.Translate(movement);
	Cam3.GetViewMatrix(view);

	XMMATRIX projection = XMMatrixPerspectiveFovLH(XM_PIDIV2, 1024.0f / 720.0f, 0.01f, 100.0f);
	XMMATRIX ortho = XMMatrixOrthographicLH(1024.0f, 720.0f, 0.0f, 1000.0f);

	float px, py, pz;
	Model2.GetPosition(px, py, pz);
	px += movement.x;
	py += movement.y;
	pz += movement.z;
	Model2.SetPosition(px, py, pz);

	pShader->SetLight(pDeviceContext, 1, &l1);

	Model.Draw(pDeviceContext, &view, &projection);
	Model2.Draw(pDeviceContext, &view, &projection);
	Plane.Draw(pDeviceContext, &view, &projection);

	if (Wnd->GetInput()->IsKeyDown(KEY_W))
	{
		static float pos = 0.0f;
		pos -= 0.05f * deltaTime;
		static float size = 40.0f;
		size += 0.05f * deltaTime;

		Picture1.SetSize((unsigned long)size, (unsigned long)size);
		Picture1.SetPosition(0, pos);
		Picture1.UpdateVertexBuffer(pDeviceContext);

		Picture2.SetSize((unsigned long)size, (unsigned long)size);
		Picture2.SetPosition(0, -pos);
		Picture2.UpdateVertexBuffer(pDeviceContext);
	}

	TurnZBufferOff();
	Picture1.Draw(pDeviceContext, &ortho);
	Picture2.Draw(pDeviceContext, &ortho);
	
	float blendFactor[4];
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	pDeviceContext->OMSetBlendState(translucency, blendFactor, 0xffffffff);

	Text1.SetText(pDeviceContext, "Test");
	Text1.Draw(pDeviceContext, &ortho);

	pDeviceContext->OMSetBlendState(0, 0, 0xffffffff);
}