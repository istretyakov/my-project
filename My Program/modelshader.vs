cbuffer ConstantBuffer
{
    matrix WVP;
	matrix World;
};

struct VS_INPUT
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
};

struct PS_INPUT
{
    float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
};

PS_INPUT VS(VS_INPUT input)
{
    PS_INPUT output;

    output.Position = mul(input.Position, WVP);
	output.Normal = mul(input.Normal, World);

    return output;
}